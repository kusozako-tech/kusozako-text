
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


VOID = 0
NO_LOCATION = 1
CHANGES_NOT_SAVED = 2
LOADING_IN_PROGRESS = 3
SAVING_IN_PROGRESS = 4
IS_EXTERNALLY_MODIFIED = 5
IS_READ_ONLY = 6
IS_DELETED = 7
SAVED = 8
