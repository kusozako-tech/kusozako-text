
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from .TerminalSequence import AlfaTerminalSequence

DIALOG_MESSAGE = "We are going to close application,\n"\
    "But Your changes not saved."

DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "dialog-warning-symbolic",
    "message": DIALOG_MESSAGE,
    "buttons": (_("Cancel"), _("Save Changes"), _("Discard Changes"))
    }


FILE_CHOOSER_MODEL = {
    "type": "save-file",
    "title": _("Save File"),
    "filters": {"Text Files": "text/*"},
    "default": _("new.txt")
    }


class DeltaHasNoFile(AlfaTerminalSequence):

    def _select_path(self):
        path = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        if path is None:
            return
        gio_file = Gio.File.new_for_path(path)
        self._raise("delta > file location defined", gio_file)
        self._save_file()

    def try_close_application(self):
        message = DIALOG_MODEL["message"]
        application_name = self._enquiry("delta > application data", "name")
        DIALOG_MODEL["message"] = message.format(application_name)
        response = DeltaMessageDialog.run_for_model(self, DIALOG_MODEL)
        if response == 1:
            self._select_path()
        if response == 2:
            self._raise("delta > application force quit")
