
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaActionReceiver(DeltaEntity):

    SIGNAL = "define signal here."

    def _action(self, param=None):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self.SIGNAL:
            self._action(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register action object", self)
