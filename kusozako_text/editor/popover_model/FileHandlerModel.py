
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals


FILE_HANDLER_MODEL = [
        {
            "type": "simple-action",
            "title": _("New"),
            "message": "delta > action",
            "user-data": (ApplicationSignals.FILE_NEW, None),
            "close-on-clicked": True,
            "shortcut": "Ctrl+N"
        },
        {
            "type": "simple-action",
            "title": _("Open"),
            "message": "delta > action",
            "user-data": (ApplicationSignals.FILE_OPEN, None),
            "close-on-clicked": True,
            "shortcut": "Ctrl+O"
        },
        {
            "type": "hidable-action",
            "title": _("Save"),
            "message": "delta > action",
            "user-data": (ApplicationSignals.FILE_SAVE, None),
            "close-on-clicked": True,
            "shortcut": "Ctrl+S",
            "registration": "delta > register application file path object",
            "query": "delta > application current path",
            "query-data": None,
            "check-value": None
        },
        {
            "type": "hidable-action",
            "title": _("Save As..."),
            "message": "delta > action",
            "user-data": (ApplicationSignals.FILE_SAVE_AS, None),
            "close-on-clicked": True,
            "shortcut": "Shift+Ctrl+S",
            "registration": "delta > register application file path object",
            "query": "delta > application current path",
            "query-data": None,
            "check-value": None,
            "check-inverted": True
        },
]
