
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FileHandlerModel import FILE_HANDLER_MODEL


MAIN_PAGE_ITEMS = [
    *FILE_HANDLER_MODEL,
    {
        "type": "switcher",
        "title": _("Recent"),
        "message": "delta > switch stack to",
        "user-data": "recent-paths",
    },
    {
        "type": "separator"
    },
    {
        "type": "switcher",
        "title": _("Editor"),
        "message": "delta > switch stack to",
        "user-data": "editor-config",
    },
]
