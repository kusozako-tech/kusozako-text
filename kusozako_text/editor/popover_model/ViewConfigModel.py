
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

VIEW_CONFIG_MODEL = {
    "page-name": "view-config",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "check-action-boolean",
            "title": _("Show Grid"),
            "message": "delta > settings",
            "user-data": ("editor_view", "show_grid", 0),
            "user-data-false": ("editor_view", "show_grid", 1),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "show_grid", 1),
            "check-value": 1
        },
        {
            "type": "check-action-boolean",
            "title": _("Show Line Numbers"),
            "message": "delta > settings",
            "user-data": ("editor_view", "show_line_numbers", False),
            "user-data-false": ("editor_view", "show_line_numbers", True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "show_line_numbers", False),
            "check-value": True
        },
        {
            "type": "check-action-boolean",
            "title": _("Highlight Current Line"),
            "message": "delta > settings",
            "user-data": ("editor_view", "highlight_current_line", False),
            "user-data-false": ("editor_view", "highlight_current_line", True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "highlight_current_line", False),
            "check-value": True
        },
        {
            "type": "check-action-boolean",
            "title": _("Highlight Matching Brackets"),
            "message": "delta > settings",
            "user-data": ("buffer-view", "highlight-brackets", False),
            "user-data-false": ("buffer-view", "highlight-brackets", True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("buffer-view", "highlight-brackets", False),
            "check-value": True
        },
    ]
}
