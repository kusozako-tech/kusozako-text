
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .RecentPathsModel import RECENT_PATHS_MODEL
from .EditorConfigModel import EDITOR_CONFIG_MODEL
from .style_schemas import StyleSchemas
from .ViewConfigModel import VIEW_CONFIG_MODEL
from .IndentConfigModel import INDENT_CONFIG_MODEL
from .WrapConfigModel import WRAP_CONFIG_MODEL

SUB_PAGES_MODEL = [
    RECENT_PATHS_MODEL,
    EDITOR_CONFIG_MODEL,
    StyleSchemas.new(),
    VIEW_CONFIG_MODEL,
    INDENT_CONFIG_MODEL,
    WRAP_CONFIG_MODEL
    ]
