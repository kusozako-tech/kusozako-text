
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

EDITOR_CONFIG_MODEL = {
    "page-name": "editor-config",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "switcher",
            "title": _("Style Schemes"),
            "message": "delta > switch stack to",
            "user-data": "style-schemas",
        },
        {
            "type": "switcher",
            "title": _("View"),
            "message": "delta > switch stack to",
            "user-data": "view-config",
        },
        {
            "type": "switcher",
            "title": _("Indent"),
            "message": "delta > switch stack to",
            "user-data": "indent-config",
        },
        {
            "type": "switcher",
            "title": _("Wrap Mode"),
            "message": "delta > switch stack to",
            "user-data": "wrap-config",
        },
        {
            "type": "simple-action",
            "title": _("Select Font"),
            "message": "delta > action",
            "user-data": "editor.select-font",
            "close-on-clicked": True,
        },
    ]
}
