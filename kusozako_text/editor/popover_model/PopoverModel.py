
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .MainPageModel import MAIN_PAGE_MODEL
from .SubPagesModel import SUB_PAGES_MODEL


def new_for_source_view():
    return [MAIN_PAGE_MODEL, *SUB_PAGES_MODEL]
