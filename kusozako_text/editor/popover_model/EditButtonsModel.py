
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals

# shortcuts implemented by source_view, not model.


EDIT_BUTTONS_MODEL = {
    "items": [
        {
            "type": "icon-button",
            "icon-name": "edit-undo-symbolic",
            "tooltip-text": _("Undo (Ctrl+Z)"),
            "message": "delta > action",
            "user-data": (ApplicationSignals.EDIT_UNDO, None),
            "close-on-clicked": False,
        },
        {
            "type": "icon-button",
            "icon-name": "edit-redo-symbolic",
            "tooltip-text": _("Redo (Ctrl+Shift+Z)"),
            "message": "delta > action",
            "user-data": (ApplicationSignals.EDIT_REDO, None),
            "close-on-clicked": False,
        },
        {
            "type": "separator"
        },
        {
            "type": "icon-button",
            "icon-name": "edit-cut-symbolic",
            "tooltip-text": _("Cut (Ctrl+X)"),
            "message": "delta > action",
            "user-data": (ApplicationSignals.EDIT_CUT, None),
            "close-on-clicked": True,
        },
        {
            "type": "icon-button",
            "icon-name": "edit-copy-symbolic",
            "tooltip-text": _("Copy (Ctrl+C)"),
            "message": "delta > action",
            "user-data": (ApplicationSignals.EDIT_COPY, None),
            "close-on-clicked": True,
        },
        {
            "type": "icon-button",
            "icon-name": "edit-paste-symbolic",
            "tooltip-text": _("paste (Ctrl+V)"),
            "message": "delta > action",
            "user-data": (ApplicationSignals.EDIT_PASTE, None),
            "close-on-clicked": True,
        },
    ]
}
