
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

RECENT_PATHS_MODEL = {
    "page-name": "recent-paths",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "recent-files",
            "display-type": "shorten",
            "query": "delta > application recent paths",
            "query-data": None,
            "close-on-clicked": True
        }
    ]
}
