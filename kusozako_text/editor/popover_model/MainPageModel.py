
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .EditButtonsModel import EDIT_BUTTONS_MODEL
from .FileHandlerModel import FILE_HANDLER_MODEL


MAIN_PAGE_MODEL = {
    "page-name": "main",
    "items": [
        {
            "type": "horizontal-box",
            "model": EDIT_BUTTONS_MODEL
        },
        {
            "type": "separator"
        },
        *FILE_HANDLER_MODEL,
        {
            "type": "switcher",
            "title": _("Recent"),
            "message": "delta > switch stack to",
            "user-data": "recent-paths",
        },
        {
            "type": "separator"
        },
        {
            "type": "switcher",
            "title": _("Editor"),
            "message": "delta > switch stack to",
            "user-data": "editor-config",
        },
        {
            "type": "check-action-boolean",
            "title": _("Show Status Bar"),
            "message": "delta > settings",
            "user-data": ("editor_view", "show_status_bar", False),
            "user-data-false": ("editor_view", "show_status_bar", True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "show_status_bar", False),
            "check-value": True
        },
        {
            "type": "check-action-boolean",
            "title": _("Show Find and Replace"),
            "message": "delta > settings",
            "user-data": ("editor_view", "show_find_and_replace", False),
            "user-data-false": ("editor_view", "show_find_and_replace", True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "show_find_and_replace", False),
            "check-value": True
        },
    ]
}
