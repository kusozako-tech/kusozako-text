
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

STYLE_SCHEMAS_MODEL = {
    "page-name": "style-schemas",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "editor-config",
        },
        {
            "type": "separator"
        },
    ]
}
