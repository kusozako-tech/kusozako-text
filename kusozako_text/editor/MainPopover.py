
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .popover_model.MainPageItems import MAIN_PAGE_ITEMS
from .popover_model.MainPageModel import MAIN_PAGE_MODEL
from .popover_model.SubPagesModel import SUB_PAGES_MODEL


class DeltaMainPopover(DeltaEntity):

    def _construct_sub_pages(self):
        for page_model in SUB_PAGES_MODEL:
            self._raise("delta > application popover add page", page_model)

    def _construct_main_page(self):
        for item_model in MAIN_PAGE_ITEMS:
            data = "main", item_model
            self._raise("delta > application popover add item", data)

    def get_model_for_source_view(self):
        return [MAIN_PAGE_MODEL, *SUB_PAGES_MODEL]

    def __init__(self, parent):
        self._parent = parent
        self._construct_main_page()
        self._construct_sub_pages()
