
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .MainPopover import DeltaMainPopover
from .content_area.ContentArea import DeltaContentArea


class DeltaEditor(DeltaEntity):

    def _delta_info_source_view_popover_model(self):
        return self._main_popover.get_model_for_source_view()

    def __init__(self, parent):
        self._parent = parent
        self._main_popover = DeltaMainPopover(self)
        DeltaContentArea(self)
