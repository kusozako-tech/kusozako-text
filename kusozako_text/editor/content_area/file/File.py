
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity
from kusozako_text.terminator.Terminator import DeltaTerminator
from .load_actions.LoadActions import DeltaLoadActions
from .save_actions.SaveActions import EchoSaveActions


class DeltaFile(GtkSource.File, DeltaEntity):

    def _delta_info_file(self):
        return self

    def _delta_call_file_location_defined(self, gio_file=None):
        self.set_location(gio_file)
        path = gio_file.get_path() if gio_file is not None else None
        self._raise("delta > application recent paths", path)
        if gio_file is not None:
            self._raise("delta > file location defined", gio_file)

    def __init__(self, parent):
        self._parent = parent
        GtkSource.File.__init__(self)
        DeltaLoadActions(self)
        EchoSaveActions(self)
        terminator = DeltaTerminator(self)
        self._raise("delta > register terminator object", terminator)
