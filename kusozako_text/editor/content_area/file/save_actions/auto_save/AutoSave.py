
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity
from .Delay import DeltaDelay


class DeltaAutoSave(DeltaEntity):

    def _on_finished(self, file_saver, task, user_data):
        if file_saver.save_finish(task):
            self._raise("delta > save finished", file_saver)

    def _on_progress(self, bytes_saved, bytes_total, user_data):
        pass
        # print("saving-in-progress", bytes_saved/bytes_total)

    def _delta_call_delay_past(self):
        file_ = self._enquiry("delta > file")
        if file_.get_location() is None:
            return
        buffer_ = self._enquiry("delta > buffer")
        file_saver = GtkSource.FileSaver.new(buffer_, file_)
        file_saver.save_async(
            GLib.PRIORITY_DEFAULT,
            None,
            self._on_progress,
            ("",),
            self._on_finished,
            ("",)
            )

    def _on_changed(self, *args):
        self._delay.start_idle()

    def __init__(self, parent):
        self._parent = parent
        buffer_ = self._enquiry("delta > buffer")
        buffer_.connect("changed", self._on_changed)
        self._delay = DeltaDelay(self)
