
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity

DELAY = 5


class DeltaDelay(DeltaEntity):

    def _on_idle(self, index):
        if self._index == index:
            self._raise("delta > delay past")
        return False

    def start_idle(self):
        index = self._index+1
        GLib.timeout_add_seconds(DELAY, self._on_idle, index)
        self._index += 1

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
