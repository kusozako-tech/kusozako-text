
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3 import ApplicationSignals
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from kusozako_text.ActionReceiver import AlfaActionReceiver

FILE_CHOOSER_MODEL = {
    "type": "save-file",
    "title": _("Save File"),
    "filters": {"Text Files": "text/*"},
    "default": _("new.txt")
    }


class DeltaSaveFileAs(AlfaActionReceiver):

    SIGNAL = ApplicationSignals.FILE_SAVE_AS

    def _save_file(self, current_path):
        model = FILE_CHOOSER_MODEL.copy()
        model["directory"] = GLib.path_get_dirname(current_path)
        model["default"] = GLib.path_get_basename(current_path)
        new_path = DeltaFileChooser.run_for_model(self, model)
        if new_path is not None:
            gio_file = Gio.File.new_for_path(new_path)
            self._raise("delta > file location defined", gio_file)

    def _action(self, param=None):
        current_path = self._enquiry("delta > application current path")
        if current_path is not None:
            self._save_file(current_path)
