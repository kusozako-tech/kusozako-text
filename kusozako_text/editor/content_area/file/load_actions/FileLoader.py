
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity


class DeltaFileLoader(DeltaEntity):

    def _on_progress(self, current_bytes, total_bytes, user_data):
        print("on-progress", current_bytes/total_bytes)

    def _on_finished(self, file_loader, task, user_data):
        if file_loader.load_finish(task):
            path = user_data[0]
            self._raise("delta > application recent paths", path)
            self._raise("delta > load finished", path)

    def _clear_content(self):
        buffer_ = self._enquiry("delta > buffer")
        buffer_.set_text("", -1)

    def _start_loading(self, path):
        file_loader = GtkSource.FileLoader.new(
            self._enquiry("delta > buffer"),
            self._enquiry("delta > file")
            )
        file_loader.load_async(
            GLib.PRIORITY_DEFAULT,
            None,
            self._on_progress,
            ("",),
            self._on_finished,
            (path,)
            )

    def load_content(self, gio_file=None):
        path = None if gio_file is None else gio_file.get_path()
        if path is None:
            self._clear_content()
        else:
            self._start_loading(path)

    def __init__(self, parent):
        self._parent = parent
