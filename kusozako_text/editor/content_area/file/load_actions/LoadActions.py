
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .StartupUri import DeltaStartupUri
from .NewFile import DeltaNewFile
from .OpenFile import DeltaOpenFile
from .FileSelected import DeltaFileSelected
from .FileLoader import DeltaFileLoader
from .terminator.Terminator import DeltaTerminator


class DeltaLoadActions(DeltaEntity):

    def _delta_call_file_location_accepted(self, gio_file=None):
        self._raise("delta > file location defined", gio_file)
        self._file_loader.load_content(gio_file)

    def _delta_call_file_selected(self, gio_file=None):
        self._terminator.can_open_new_file(gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._terminator = DeltaTerminator(self)
        self._file_loader = DeltaFileLoader(self)
        DeltaNewFile(self)
        DeltaOpenFile(self)
        DeltaFileSelected(self)
        DeltaStartupUri(self)
