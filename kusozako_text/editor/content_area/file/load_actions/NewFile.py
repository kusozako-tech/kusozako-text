
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals
from kusozako_text.ActionReceiver import AlfaActionReceiver


class DeltaNewFile(AlfaActionReceiver):

    SIGNAL = ApplicationSignals.FILE_NEW

    def _action(self, param=None):
        self._raise("delta > file selected", None)
