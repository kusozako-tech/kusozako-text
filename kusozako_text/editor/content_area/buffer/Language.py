
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity


class DeltaLanguage(DeltaEntity):

    def receive_transmission(self, user_data):
        type_, path = user_data
        if type_ != "path":
            return
        buffer_ = self._enquiry("delta > buffer")
        language = self._manager.guess_language(path)
        buffer_.set_language(language)

    def __init__(self, parent):
        self._parent = parent
        self._manager = GtkSource.LanguageManager.get_default()
        self._raise("delta > register application file path object", self)
