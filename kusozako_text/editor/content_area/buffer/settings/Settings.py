
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .StyleScheme import DeltaStyleScheme
from .HighlightMatchingBrackets import DeltaHighlightMatchingBrackets


class EchoSettings:

    def __init__(self, parent):
        DeltaStyleScheme(parent)
        DeltaHighlightMatchingBrackets(parent)
