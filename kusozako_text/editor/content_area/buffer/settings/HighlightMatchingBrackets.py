
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaHighlightMatchingBrackets(DeltaEntity):

    def _reset(self, value):
        query = "buffer-view", "highlight-matching-brackets", value
        current_value = self._enquiry("delta > settings", query)
        buffer_ = self._enquiry("delta > buffer")
        buffer_.set_property("highlight-matching-brackets", current_value)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "buffer-view" and key == "highlight-matching-brackets":
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        self._reset(True)
        self._raise("delta > register settings object", self)
