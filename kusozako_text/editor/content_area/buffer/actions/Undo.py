
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals
from kusozako_text.ActionReceiver import AlfaActionReceiver


class DeltaUndo(AlfaActionReceiver):

    SIGNAL = ApplicationSignals.EDIT_UNDO

    def _action(self, param=None):
        buffer_ = self._enquiry("delta > buffer")
        buffer_.undo()
