
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SelectFont import DeltaSelectFont
from .Undo import DeltaUndo
from .Redo import DeltaRedo
from .clipboard_actions.ClipboardActions import EchoClipboardActions


class EchoActions:

    def __init__(self, parent):
        DeltaSelectFont(parent)
        DeltaUndo(parent)
        DeltaRedo(parent)
        EchoClipboardActions(parent)
