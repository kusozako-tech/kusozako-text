
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Cut import DeltaCut
from .Copy import DeltaCopy
from .Paste import DeltaPaste


class EchoClipboardActions:

    def __init__(self, parent):
        DeltaCut(parent)
        DeltaCopy(parent)
        DeltaPaste(parent)
