
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals
from kusozako_text.ActionReceiver import AlfaActionReceiver


class DeltaCut(AlfaActionReceiver):

    SIGNAL = ApplicationSignals.EDIT_CUT

    def _action(self, param=None):
        buffer_ = self._enquiry("delta > buffer")
        buffer_.cut_clipboard(self._clipboard, True)
