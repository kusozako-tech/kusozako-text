
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.font_chooser.FontChooser import DeltaFontChooser


class DeltaSelectFont(DeltaEntity):

    def _action(self):
        query = "css", "editor_font_name", ""
        current_css_font = self._enquiry("delta > settings", query)
        selected_font = DeltaFontChooser.get_css_font(self, current_css_font)
        if selected_font is None:
            return
        settings = "css", "editor_font_name", selected_font
        self._raise("delta > settings", settings)

    def receive_transmission(self, action_id):
        if action_id == "editor.select-font":
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register action object", self)
