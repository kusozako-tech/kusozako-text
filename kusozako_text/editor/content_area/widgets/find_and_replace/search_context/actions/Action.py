
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaAction(DeltaEntity):

    SIGNAL = "define acceptable signal here."

    def _move_cursor(self, buffer_, match_start, match_end):
        buffer_.select_range(match_start, match_end)
        view = self._enquiry("delta > view")
        view.scroll_to_iter(match_start, 0, False, 0, 0)

    def _action(self):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self.SIGNAL:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register search object", self)
