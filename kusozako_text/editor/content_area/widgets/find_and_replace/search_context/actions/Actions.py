
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SearchForward import DeltaSearchForward
from .SearchBackward import DeltaSearchBackward
from .Replace import DeltaReplace
from .ReplaceAll import DeltaReplaceAll


class EchoActions:

    def __init__(self, parent):
        DeltaSearchForward(parent)
        DeltaSearchBackward(parent)
        DeltaReplace(parent)
        DeltaReplaceAll(parent)
