
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Action import AlfaAction
from kusozako_text import SearchSignals

BACKWARD = 0
FORWARD = 1


class DeltaSearchBackward(AlfaAction):

    SIGNAL = SearchSignals.SEARCH_PREVIOUS

    def _get_current_iter_for_buffuer(self, buffer_):
        bounds = buffer_.get_selection_bounds()
        if bounds:
            position = bounds[BACKWARD].get_offset()
        else:
            position = buffer_.get_property("cursor-position")
        return buffer_.get_iter_at_offset(position)

    def _action(self):
        buffer_ = self._enquiry("delta > buffer")
        iter_ = self._get_current_iter_for_buffuer(buffer_)
        search_context = self._enquiry("delta > search context")
        found, match_start, match_end, _ = search_context.backward(iter_)
        if not found:
            return
        self._move_cursor(buffer_, match_start, match_end)
