
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .grid.Grid import DeltaGrid


class DeltaWidgets(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _reset(self, reveal_child):
        self._revealer.set_reveal_child(reveal_child)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "editor_view" and key == "show_find_and_replace":
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        self._revealer = Gtk.Revealer()
        Gtk.Box.__init__(self)
        self._revealer.add(self)
        DeltaGrid(self)
        query = "editor_view", "show_find_and_replace", False
        value = self._enquiry("delta > settings", query)
        self._reset(value)
        self._raise("delta > add to container", self._revealer)
        self._raise("delta > register settings object", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
