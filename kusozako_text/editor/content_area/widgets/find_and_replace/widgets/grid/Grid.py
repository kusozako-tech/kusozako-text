
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .CloseButton import DeltaCloseButton
from .SearchText import DeltaSearchText
from .ReplacementText import DeltaReplacementText
from .PreviousButton import DeltaPreviousButton
from .ReplaceButton import DeltaReplaceButton
from .NextButton import DeltaNextButton
from .ReplaceAllButton import DeltaReplaceAllButton


class DeltaGrid(Gtk.Grid, DeltaEntity):

    def _delta_call_attach_to_grid(self, user_data):
        widget, geometries = user_data
        self.attach(widget, *geometries)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(
            self,
            margin=Unit(1),
            column_spacing=Unit(1),
            row_spacing=Unit(1)
            )
        DeltaCloseButton(self)
        DeltaSearchText(self)
        DeltaReplacementText(self)
        DeltaPreviousButton(self)
        DeltaNextButton(self)
        DeltaReplaceButton(self)
        DeltaReplaceAllButton(self)
        self._raise("delta > add to container", self)
