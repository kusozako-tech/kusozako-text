
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_text import SearchSignals


class DeltaReplaceButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = SearchSignals.REPLACE, None
        self._raise("delta > search signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, _("Replace"))
        self.connect("clicked", self._on_clicked)
        self._raise("delta > attach to grid", (self, (2, 1, 1, 1)))
