
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_text import SearchSignals


class DeltaSearchText(Gtk.SearchEntry, DeltaEntity):

    def _on_changed(self, entry):
        user_data = SearchSignals.SEARCH_TEXT_CHANGED, entry.get_text()
        self._raise("delta > search signal", user_data)

    def _on_activate(self, entry):
        user_data = SearchSignals.SEARCH_NEXT, None
        self._raise("delta > search signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(
            self,
            placeholder_text=_("Text to Search"),
            hexpand=True
            )
        self.connect("changed", self._on_changed)
        self.connect("activate", self._on_activate)
        self._raise("delta > attach to grid", (self, (1, 0, 1, 1)))
