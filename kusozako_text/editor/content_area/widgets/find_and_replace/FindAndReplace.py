
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .search_context.SearchContext import DeltaSearchContext
from .widgets.Widgets import DeltaWidgets


class DeltaFindAndReplace(DeltaEntity):

    def _delta_call_search_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_search_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaSearchContext(self)
        DeltaWidgets(self)
