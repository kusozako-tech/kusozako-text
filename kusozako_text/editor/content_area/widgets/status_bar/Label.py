
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

TEMPLATE = _("lines {}/{}, column {}")


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _on_changed(self, buffer_):
        cursor_position = buffer_.props.cursor_position
        iter_ = buffer_.get_iter_at_offset(cursor_position)
        lines = buffer_.get_line_count()
        line_offset = iter_.get_line_offset()
        status = iter_.get_line()+1, lines, line_offset
        self.set_text(TEMPLATE.format(*status))
        # self._raise("delta > cursor moved", status)

    def _on_move_cursor(self, view, step, count, extend_selection, buffer_):
        self._on_changed(buffer_)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_size_request(-1, Unit(5))
        buffer_ = self._enquiry("delta > buffer")
        buffer_.connect("changed", self._on_changed)
        view = self._enquiry("delta > view")
        view.connect("move-cursor", self._on_move_cursor, buffer_)
        self._on_changed(buffer_)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
