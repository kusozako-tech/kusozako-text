
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsHandler import AlfaSettingsHandler


class DeltaIndentWidth(AlfaSettingsHandler):

    PROPERTY_NAME = "indent-width"
    KEY = "indent_width"
    DEFAULT = 4
