
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsHandler import AlfaSettingsHandler


class DeltaShowLineNumbers(AlfaSettingsHandler):

    PROPERTY_NAME = "show-line-numbers"
    KEY = "show_line_numbers"
    DEFAULT = False
