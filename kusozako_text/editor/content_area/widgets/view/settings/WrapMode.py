
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .SettingsHandler import AlfaSettingsHandler


class DeltaWrapMode(AlfaSettingsHandler):

    PROPERTY_NAME = "wrap-mode"
    KEY = "wrap_mode"
    # DEFAULT = Gtk.WrapMode.NONE
    DEFAULT = 0
