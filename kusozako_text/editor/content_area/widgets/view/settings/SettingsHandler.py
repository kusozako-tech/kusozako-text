
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaSettingsHandler(DeltaEntity):

    PROPERTY_NAME = "define property name here."
    KEY = "define settings key here"
    DEFAULT = "define default value here."

    def _reset(self, value):
        view = self._enquiry("delta > view")
        view.set_property(self.PROPERTY_NAME, value)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "editor_view" and key == self.KEY:
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        query = "editor_view", self.KEY, self.DEFAULT
        value = self._enquiry("delta > settings", query)
        self._reset(value)
        self._raise("delta > register settings object", self)
