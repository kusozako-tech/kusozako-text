
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsHandler import AlfaSettingsHandler


class DeltaInsertSpacesInsteadOfTabs(AlfaSettingsHandler):

    PROPERTY_NAME = "insert-spaces-instead-of-tabs"
    KEY = "insert_spaces_instead_of_tabs"
    DEFAULT = True
