
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Map import DeltaMap
from .ButtonPressEvent import DeltaButtonPressEvent


class EchoWatchers:

    def __init__(self, parent):
        DeltaMap(parent)
        DeltaButtonPressEvent(parent)
