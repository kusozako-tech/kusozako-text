
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu


class DeltaButtonPressEvent(DeltaEntity):

    def _on_button_press(self, view, event, popover):
        # this function returns True to override impremented context-menu
        if event.button != 3:
            return False
        # adjust horizontal position for lne numbers window
        gdk_window = view.get_window(Gtk.TextWindowType.LEFT)
        left_adjustment = 0 if gdk_window is None else gdk_window.get_width()
        popup_x = event.x+left_adjustment
        popover.popup_for_position(view, popup_x, event.y)
        return True

    def __init__(self, parent):
        self._parent = parent
        model = self._enquiry("delta > source view popover model")
        popover = DeltaPopoverMenu.new_for_model(self, model)
        view = self._enquiry("delta > view")
        view.connect("button-press-event", self._on_button_press, popover)
