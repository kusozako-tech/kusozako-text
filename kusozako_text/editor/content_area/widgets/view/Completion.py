
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity


class DeltaCompletion(GtkSource.CompletionWords, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        GtkSource.CompletionWords.__init__(self, name="auto-completion")
        view = self._enquiry("delta > view")
        view_completion = view.get_completion()
        buffer_ = self._enquiry("delta > buffer")
        self.register(buffer_)
        view_completion.add_provider(self)
