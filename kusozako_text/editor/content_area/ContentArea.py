
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .buffer.Buffer import DeltaBuffer
from .file.File import DeltaFile
from .widgets.Widgets import DeltaWidgets


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_info_buffer(self):
        return self._buffer

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._buffer = DeltaBuffer(self)
        DeltaWidgets(self)
        DeltaFile(self)
        self._raise("delta > add to container", self)
