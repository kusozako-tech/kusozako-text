
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SEARCH_TEXT_CHANGED = 0             # str as new search text
REPLACEMENT_TEXT_CHANGED = 1        # str as new replacement text
SEARCH_NEXT = 2                     # NONE
SEARCH_PREVIOUS = 3                 # NONE
REPLACE = 4                         # NONE
REPLACE_ALL = 5                     # NONE
