
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .HasFile import DeltaHasFile
from .HasNoFile import DeltaHasNoFile


class DeltaTerminator(DeltaEntity):

    def is_closable(self):
        current_path = self._enquiry("delta > application current path")
        buffer_ = self._enquiry("delta > buffer")
        if current_path is None and buffer_.props.text != "":
            self._has_no_file.try_close_application()
        else:
            self._has_file.try_close_application()
        return False

    def __init__(self, parent):
        self._parent = parent
        self._has_file = DeltaHasFile(self)
        self._has_no_file = DeltaHasNoFile(self)
