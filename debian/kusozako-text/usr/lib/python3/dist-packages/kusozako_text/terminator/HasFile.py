
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog
from .TerminalSequence import AlfaTerminalSequence

DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "dialog-question-symbolic",
    "message": "Close {} ?",
    "buttons": (_("Cancel"), _("Close kusozako-text"))
    }


class DeltaHasFile(AlfaTerminalSequence):

    def try_close_application(self):
        message = DIALOG_MODEL["message"]
        application_name = self._enquiry("delta > application data", "name")
        DIALOG_MODEL["message"] = message.format(application_name)
        response = DeltaMessageDialog.run_for_model(self, DIALOG_MODEL)
        if response != 0:
            self._save_file()
