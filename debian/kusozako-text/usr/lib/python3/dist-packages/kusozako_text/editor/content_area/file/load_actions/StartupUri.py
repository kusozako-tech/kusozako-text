
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaStartupUri(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        files = self._enquiry("delta > command line files")
        if files:
            self._raise("delta > file selected", files[0])
