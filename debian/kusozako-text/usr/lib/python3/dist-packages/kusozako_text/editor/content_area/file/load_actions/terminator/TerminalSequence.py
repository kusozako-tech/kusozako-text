
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity


class AlfaTerminalSequence(DeltaEntity):

    def _on_finished(self, file_saver, task, user_data):
        success = file_saver.save_finish(task)
        print("success ?", success)
        self._raise("delta > file location accepted", user_data[0])

    def _on_progress(self, bytes_saved, bytes_total, user_data):
        print("saving-in-progress", bytes_saved/bytes_total)

    def _save_file(self, gio_file=None):
        file_ = self._enquiry("delta > file")
        buffer_ = self._enquiry("delta > buffer")
        file_saver = GtkSource.FileSaver.new(buffer_, file_)
        file_saver.save_async(
            GLib.PRIORITY_DEFAULT,
            None,
            self._on_progress,
            ("",),
            self._on_finished,
            (gio_file,)
            )

    def __init__(self, parent):
        self._parent = parent
