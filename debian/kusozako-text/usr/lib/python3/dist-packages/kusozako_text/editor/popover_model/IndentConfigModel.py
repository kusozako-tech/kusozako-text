
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

INSTEAD_OF_TABS = "insert_spaces_instead_of_tabs"

INDENT_CONFIG_MODEL = {
    "page-name": "indent-config",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "check-action-boolean",
            "title": _("Auto Indent"),
            "message": "delta > settings",
            "user-data": ("editor_view", "auto_indent", False),
            "user-data-false": ("editor_view", "auto_indent", True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "auto_indent", True),
            "check-value": True
        },
        {
            "type": "check-action-boolean",
            "title": _("Insert Spaces Instead of Tabs"),
            "message": "delta > settings",
            "user-data": ("editor_view", INSTEAD_OF_TABS, False),
            "user-data-false": ("editor_view", INSTEAD_OF_TABS, True),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", INSTEAD_OF_TABS, False),
            "check-value": True
        },
        {
            "type": "spin-box",
            "title": _("indent width"),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "message": "delta > settings",
            "user-data": ("editor_view", "indent_width"),
            "query": "delta > settings",
            "query-data": ("editor_view", "indent_width", 4),
            "increments": (1, 1),
            "range": (2, 16)
        },
    ]
}
