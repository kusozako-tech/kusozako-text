
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_text import SearchSignals


class DeltaReplacementText(Gtk.SearchEntry, DeltaEntity):

    def _on_changed(self, entry):
        user_data = SearchSignals.REPLACEMENT_TEXT_CHANGED, entry.get_text()
        self._raise("delta > search signal", user_data)

    def _on_activate(self, entry):
        user_data = SearchSignals.REPLACE, None
        self._raise("delta > search signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(
            self,
            hexpand=True,
            placeholder_text=_("Replacement"),
            primary_icon_name="edit-find-replace-symbolic"
            )
        self.connect("search-changed", self._on_changed)
        self.connect("activate", self._on_activate)
        self._raise("delta > attach to grid", (self, (1, 1, 1, 1)))
