
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity


class DeltaStyleScheme(DeltaEntity):

    def _reset(self, value):
        query = "buffer-view", "style-scheme", value
        current_value = self._enquiry("delta > settings", query)
        buffer_ = self._enquiry("delta > buffer")
        scheme = self._manager.get_scheme(current_value)
        buffer_.set_property("style-scheme", scheme)

    def _on_initialize(self):
        self._manager = GtkSource.StyleSchemeManager.get_default()

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "buffer-view" and key == "style-scheme":
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        self._reset("classic")
        self._raise("delta > register settings object", self)
