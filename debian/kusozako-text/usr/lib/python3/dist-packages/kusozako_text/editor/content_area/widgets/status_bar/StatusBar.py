
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Label import DeltaLabel


class DeltaStatusBar(Gtk.Revealer, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _reset(self, reveal_child):
        self.set_reveal_child(reveal_child)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "editor_view" and key == "show_status_bar":
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        DeltaLabel(self)
        query = "editor_view", "show_status_bar", False
        value = self._enquiry("delta > settings", query)
        self._reset(value)
        self._raise("delta > add to container", self)
        self._raise("delta > register settings object", self)
