
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# Gtk.WrapMod.NONE = 0
# Gtk.WrapMod..CHAR = 1
# Gtk.WrapMod.WORD_CHAR = 3
# see: https://lazka.github.io/pgi-docs/#Gtk-3.0/enums.html#Gtk.WrapMode

WRAP_CONFIG_MODEL = {
    "page-name": "wrap-config",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "check-action",
            "title": _("None"),
            "message": "delta > settings",
            "user-data": ("editor_view", "wrap_mode", 0),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "wrap_mode", 0),
            "check-value": 0
        },
        {
            "type": "check-action",
            "title": _("Charactor"),
            "message": "delta > settings",
            "user-data": ("editor_view", "wrap_mode", 1),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "wrap_mode", 0),
            "check-value": 1
        },
        {
            "type": "check-action",
            "title": _("Words"),
            "message": "delta > settings",
            "user-data": ("editor_view", "wrap_mode", 3),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("editor_view", "wrap_mode", 0),
            "check-value": 3
        },
    ]
}
