
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

ITEM_MODEL = {
    "type": "check-action",
    "title": _("Visible Name"),
    "message": "delta > settings",
    "user-data": ("group", "key", "value"),
    "close-on-clicked": False,
    "registration": "delta > register settings object",
    "query": "delta > settings",
    "query-data": ("group", "key", "default"),
    "check-value": "id_"
}
