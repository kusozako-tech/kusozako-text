
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_text import SearchSignals
from .Action import AlfaAction


class DeltaReplaceAll(AlfaAction):

    SIGNAL = SearchSignals.REPLACE_ALL

    def _get_matched_iters(self, buffer_, search_context):
        bounds = buffer_.get_selection_bounds()
        if bounds:
            return True, *bounds, ""
        position = buffer_.get_property("cursor-position")
        iter_ = buffer_.get_iter_at_offset(position)
        return search_context.forward(iter_)

    def _action(self):
        buffer_ = self._enquiry("delta > buffer")
        search_context = self._enquiry("delta > search context")
        result = self._get_matched_iters(buffer_, search_context)
        found, match_start, match_end, _ = result
        if not found:
            return
        self._move_cursor(buffer_, match_start, match_end)
        search_context.replace_all(
            self._enquiry("delta > replacement text"),
            -1
            )
