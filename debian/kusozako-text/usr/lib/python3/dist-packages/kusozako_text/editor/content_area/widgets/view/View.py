
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity
from .settings.Settings import EchoSettings
from .watchers.Watchers import EchoWatchers
from .Completion import DeltaCompletion


class DeltaView(GtkSource.View, DeltaEntity):

    def _delta_info_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        GtkSource.View.__init__(self, vexpand=True)
        self.set_buffer(self._enquiry("delta > buffer"))
        EchoSettings(self)
        EchoWatchers(self)
        DeltaCompletion(self)
        scrolled_window.add(self)
        self._raise("delta > add to container", scrolled_window)
