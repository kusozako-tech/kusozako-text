
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SaveFile import DeltaSaveFile
from .SaveFileAs import DeltaSaveFileAs
from .auto_save.AutoSave import DeltaAutoSave


class EchoSaveActions:

    def __init__(self, parent):
        DeltaSaveFile(parent)
        DeltaSaveFileAs(parent)
        DeltaAutoSave(parent)
