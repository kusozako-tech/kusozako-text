
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaAction(DeltaEntity):

    ACTION_ID = "define acceptable action is here."

    def _action(self):
        raise NotImplementedError

    def receive_transmission(self, action_id):
        if action_id == self.ACTION_ID:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register action object", self)
