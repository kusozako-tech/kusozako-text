
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity
from kusozako_text import SearchSignals


class DeltaSearchSettings(GtkSource.SearchSettings, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != SearchSignals.SEARCH_TEXT_CHANGED:
            return
        self.set_search_text(param)

    def __init__(self, parent):
        self._parent = parent
        GtkSource.SearchSettings.__init__(
            self,
            wrap_around=True,
            case_sensitive=False
            )
        self._raise("delta > register search object", self)
