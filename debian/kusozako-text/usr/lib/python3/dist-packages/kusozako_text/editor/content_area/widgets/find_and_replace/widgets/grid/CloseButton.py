
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaCloseButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = "editor_view", "show_find_and_replace", False
        self._raise("delta > settings", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, _("Close"))
        self.connect("clicked", self._on_clicked)
        self._raise("delta > attach to grid", (self, (0, 0, 1, 2)))
