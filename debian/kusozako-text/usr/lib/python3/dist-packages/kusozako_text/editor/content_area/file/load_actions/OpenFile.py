
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3 import ApplicationSignals
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from kusozako_text.ActionReceiver import AlfaActionReceiver

FILE_CHOOSER_MODEL = {
    "type": "select-file",
    "title": _("Select Text File"),
    "filters": {"Text Files": "text/*"}
    }


class DeltaOpenFile(AlfaActionReceiver):

    SIGNAL = ApplicationSignals.FILE_OPEN

    def _action(self, param=None):
        path = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        if path is not None:
            gio_file = Gio.File.new_for_path(path)
            self._raise("delta > file selected", gio_file)
