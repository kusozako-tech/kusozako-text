
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_text import SearchSignals


class DeltaReplacementText(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == SearchSignals.REPLACEMENT_TEXT_CHANGED:
            self._text = param

    def __init__(self, parent):
        self._parent = parent
        self._text = ""
        self._raise("delta > register search object", self)

    @property
    def text(self):
        return self._text
