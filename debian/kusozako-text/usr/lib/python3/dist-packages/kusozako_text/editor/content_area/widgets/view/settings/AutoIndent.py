
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsHandler import AlfaSettingsHandler


class DeltaAutoIndent(AlfaSettingsHandler):

    PROPERTY_NAME = "auto-indent"
    KEY = "auto_indent"
    DEFAULT = True
