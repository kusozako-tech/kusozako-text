
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals
from kusozako_text.ActionReceiver import AlfaActionReceiver


class DeltaFileSelected(AlfaActionReceiver):

    SIGNAL = ApplicationSignals.FILE_SELECTED

    def _action(self, gio_file):
        self._raise("delta > file selected", gio_file)
