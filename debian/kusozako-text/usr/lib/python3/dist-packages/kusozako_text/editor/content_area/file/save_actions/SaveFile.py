
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GtkSource
from gi.repository import Gio
from libkusozako3 import ApplicationSignals
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from kusozako_text.ActionReceiver import AlfaActionReceiver

FILE_CHOOSER_MODEL = {
    "type": "save-file",
    "title": _("Save File"),
    "filters": {"Text Files": "text/*"},
    "default": _("new.txt")
    }


class DeltaSaveFile(AlfaActionReceiver):

    SIGNAL = ApplicationSignals.FILE_SAVE

    def _save_file(self):
        path = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        if path is not None:
            gio_file = Gio.File.new_for_path(path)
            self._raise("delta > file location defined", gio_file)

    def _on_finished(self, file_saver, task, user_data):
        if file_saver.save_finish(task):
            self._raise("delta > save finished", file_saver)

    def _force_save(self, file_):
        buffer_ = self._enquiry("delta > buffer")
        file_saver = GtkSource.FileSaver.new(buffer_, file_)
        file_saver.save_async(
            GLib.PRIORITY_DEFAULT,
            None,
            None,
            ("",),
            self._on_finished,
            ("",)
            )

    def _action(self, param=None):
        file_ = self._enquiry("delta > file")
        if file_.get_location() is None:
            self._save_file()
        else:
            self._force_save(file_)
