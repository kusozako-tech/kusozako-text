
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsHandler import AlfaSettingsHandler


class DeltaShowGrid(AlfaSettingsHandler):

    PROPERTY_NAME = "background-pattern"
    KEY = "show_grid"
    DEFAULT = 1
