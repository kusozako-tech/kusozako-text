
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity
from .actions.Actions import EchoActions
from .SearchSettings import DeltaSearchSettings
from .ReplacementText import DeltaReplacementText


class DeltaSearchContext(GtkSource.SearchContext, DeltaEntity):

    def _delta_info_search_context(self):
        return self

    def _delta_info_replacement_text(self):
        return self._replacement_text.text

    def __init__(self, parent):
        self._parent = parent
        GtkSource.SearchContext.__init__(
            self,
            buffer=self._enquiry("delta > buffer"),
            settings=DeltaSearchSettings(self)
            )
        self._replacement_text = DeltaReplacementText(self)
        EchoActions(self)
