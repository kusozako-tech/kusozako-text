
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from .StyleSchemasModel import STYLE_SCHEMAS_MODEL
from .ItemModel import ITEM_MODEL


def new():
    style_schemas_model = STYLE_SCHEMAS_MODEL.copy()
    manager = GtkSource.StyleSchemeManager.get_default()
    for id_ in manager.get_scheme_ids():
        scheme = manager.get_scheme(id_)
        item = ITEM_MODEL.copy()
        item["title"] = _(scheme.get_name())
        item["user-data"] = ("buffer-view", "style-scheme", id_)
        item["query-data"] = ("buffer-view", "style-scheme", "classic")
        item["check-value"] = id_
        style_schemas_model["items"].append(item)
    return style_schemas_model
