
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals
from kusozako_text.ActionReceiver import AlfaActionReceiver


class DeltaCopy(AlfaActionReceiver):

    SIGNAL = ApplicationSignals.EDIT_COPY

    def _action(self, param=None):
        buffer_ = self._enquiry("delta > buffer")
        buffer_.copy_clipboard(self._clipboard)
