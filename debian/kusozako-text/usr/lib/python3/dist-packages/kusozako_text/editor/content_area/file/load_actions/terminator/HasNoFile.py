
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from .TerminalSequence import AlfaTerminalSequence

DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "dialog-warning-symbolic",
    "message": "We are going to open new file,\nBut Your changes not saved.",
    "buttons": (_("Cancel"), _("Save Changes"), _("Discard Changes"))
    }


FILE_CHOOSER_MODEL = {
    "type": "save-file",
    "title": _("Save File"),
    "filters": {"Text Files": "text/*"},
    "default": _("new.txt")
    }


class DeltaHasNoFile(AlfaTerminalSequence):

    def _select_path(self, gio_file_to_load=None):
        path = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        if path is None:
            return
        gio_file_to_save = Gio.File.new_for_path(path)
        self._raise("delta > file location defined", gio_file_to_save)
        self._save_file(gio_file_to_load)

    def try_save(self, gio_file=None):
        message = DIALOG_MODEL["message"]
        application_name = self._enquiry("delta > application data", "name")
        DIALOG_MODEL["message"] = message.format(application_name)
        response = DeltaMessageDialog.run_for_model(self, DIALOG_MODEL)
        if response == 1:
            self._select_path()
        if response == 2:
            self._raise("delta > location accepted", gio_file)
