
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .TerminalSequence import AlfaTerminalSequence


class DeltaHasFile(AlfaTerminalSequence):

    def try_save(self, gio_file=None):
        self._save_file(gio_file)
