
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .view.View import DeltaView
from .status_bar.StatusBar import DeltaStatusBar
from .find_and_replace.FindAndReplace import DeltaFindAndReplace


class DeltaWidgets(DeltaEntity):

    def _delta_info_view(self):
        return self._view

    def __init__(self, parent):
        self._parent = parent
        self._view = DeltaView(self)
        DeltaStatusBar(self)
        DeltaFindAndReplace(self)
