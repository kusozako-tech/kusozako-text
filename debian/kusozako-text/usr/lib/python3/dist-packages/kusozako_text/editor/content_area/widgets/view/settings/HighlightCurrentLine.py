
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsHandler import AlfaSettingsHandler


class DeltaHighlightCurrentLine(AlfaSettingsHandler):

    PROPERTY_NAME = "highlight-current-line"
    KEY = "highlight_current_line"
    DEFAULT = False
