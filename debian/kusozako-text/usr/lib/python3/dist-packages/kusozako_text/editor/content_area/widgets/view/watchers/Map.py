
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaMap(DeltaEntity):

    def _on_map(self, view):
        view.grab_focus()

    def __init__(self, parent):
        self._parent = parent
        view = self._enquiry("delta > view")
        view.connect("map", self._on_map)
