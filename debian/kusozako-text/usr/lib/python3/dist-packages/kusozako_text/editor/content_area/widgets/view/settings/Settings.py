
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ShowGrid import DeltaShowGrid
from .ShowLineNumbers import DeltaShowLineNumbers
from .HighlightCurrentLine import DeltaHighlightCurrentLine
from .AutoIndent import DeltaAutoIndent
from .InsertSpacesInsteadOfTabs import DeltaInsertSpacesInsteadOfTabs
from .IndentWidth import DeltaIndentWidth
from .WrapMode import DeltaWrapMode


class EchoSettings:

    def __init__(self, parent):
        DeltaShowGrid(parent)
        DeltaShowLineNumbers(parent)
        DeltaHighlightCurrentLine(parent)
        DeltaAutoIndent(parent)
        DeltaInsertSpacesInsteadOfTabs(parent)
        DeltaIndentWidth(parent)
        DeltaWrapMode(parent)
