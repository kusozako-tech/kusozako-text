
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity
from .Language import DeltaLanguage
from .settings.Settings import EchoSettings
from .actions.Actions import EchoActions


class DeltaBuffer(GtkSource.Buffer, DeltaEntity):

    def _delta_info_buffer(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        GtkSource.Buffer.__init__(self)
        DeltaLanguage(self)
        EchoSettings(self)
        EchoActions(self)
